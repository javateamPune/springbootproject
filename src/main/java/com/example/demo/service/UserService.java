package com.example.demo.service;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.dao.UserDao;
import com.example.demo.model.User;

@Service
public class UserService {
	Logger log = Logger.getLogger(UserService.class);

	@Autowired
	UserDao userDao;

  public boolean  saveUser(User user){	  
       	userDao.save(user) ;
       	return true;
    }
	
  public User getUserByEmail(String email) {
	  return null;
	//  return userDao.findById(email);
  }
  
  public boolean isValidUser(User user) {
	   return userDao.findById(user.getEmail()).get().getPass().equals(user.getPass())?true:false;
	  
  } 

}
