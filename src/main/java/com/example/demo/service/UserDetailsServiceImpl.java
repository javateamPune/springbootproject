package com.example.demo.service;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.example.demo.dao.UserDao;
import com.example.demo.model.User;
import com.example.demo.repo.UserRepo;

@Service
public class UserDetailsServiceImpl implements UserDetailsService{

	static final Logger log = Logger.getLogger(UserDetailsServiceImpl.class);
	@Autowired
	UserDao userDao;
	
	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        User user =  userDao.findById(username).get();
		if(user==null || user.getEmail().isEmpty()) {
			throw new UsernameNotFoundException("User not found");
		}
		log.info(" User: "+user);
		return new UserRepo(user);		
	}
	
}
