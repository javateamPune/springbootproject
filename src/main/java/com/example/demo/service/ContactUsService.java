package com.example.demo.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.dao.ContactUsDao;
import com.example.demo.model.ContactUs;

@Service
public class ContactUsService {

  @Autowired
  ContactUsDao contactUsDao; 
  
  public void saveContactUs(ContactUs contactUs) {
	  
	  contactUsDao.save(contactUs);
  }
	
	
}
