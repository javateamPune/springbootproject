package com.example.demo.service;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.dao.AdvertiseDao;
import com.example.demo.model.Advertise;

@Service
public class AdvertiseService {

	Logger log = Logger.getLogger(AdvertiseService.class);

	@Autowired
	AdvertiseDao advertiseDao;

	public Advertise saveAdd(Advertise advertise) throws Exception {
		//log.info(advertise);
		//advertise.setAddId("addone");
		return advertiseDao.save(advertise);
	}

	public Iterable<Advertise> getAllAdds() throws Exception {

		return advertiseDao.findAll();
	}

	public Advertise getAddById(String id) throws Exception {
       
		return advertiseDao.findById(id).get();
	}

}
