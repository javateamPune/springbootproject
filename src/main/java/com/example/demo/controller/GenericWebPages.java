package com.example.demo.controller;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class GenericWebPages {
    Logger log = Logger.getLogger(GenericWebPages.class);

	@RequestMapping({"/index","/"})
	public String  getWelcomePage(Model model,HttpServletRequest request) throws IOException {	
		
		String action = request.getUserPrincipal()!=null? "Logout":"Login";		
		model.addAttribute("action",action);
		return "index";
	}
	
	@RequestMapping("/header")
	public String getHeaderPage(HttpServletRequest req) throws IOException {
		
		return "header";
	}
	
	@RequestMapping("/footer")
	public String getFooterPage() throws IOException {
		return "footer";
	}
	
	@GetMapping("/login")
	public String getLoginPage() throws IOException {
		return "login";
	}
	

	@RequestMapping("/register")
	public String getRegisterPage() throws IOException {
		return "register";
	}

	
	@RequestMapping("/forgotpassword")
	public String getForgotpasswordPage() throws IOException {
		return "forgotpassword";
	}
	
	@RequestMapping("/adminview")
	public String getAdminViewPage() throws IOException {
		return "adminview";
	}
	

	
	@RequestMapping("/demo")
	public String getDemoPage() throws IOException {
		return "Demo";
	}
	
	
	
}
