package com.example.demo.controller;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.example.demo.model.User;
import com.example.demo.service.UserService;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;

@Controller
public class UserController {
	Logger log = Logger.getLogger(UserController.class);
	ModelAndView mv = null;
	@Autowired
	private UserService us;

	@GetMapping("/")
	public String getUserLoginForm() {

		return "index";
	}

	@PostMapping(value = "/login")
	public ModelAndView getLoginPage(User user) {
		
		mv = new ModelAndView();
		if (us.isValidUser(user)) {
			log.info("logged in");
			mv.setViewName("index");
		} else {

			mv = new ModelAndView("login");
			mv.addObject("msg", "User name or password is invalid !!!");
		}

		return mv;
	}

	@RequestMapping(value = "/logout")
	public ModelAndView logoutUser(HttpServletRequest req) {

		mv = new ModelAndView("login");
		mv.addObject("msg", "Logged out sucessfully... ");
		return mv;
	}

	@PostMapping(value = "/register")
	public ModelAndView loginUser(User user) {
	    mv = new ModelAndView("index");
		us.saveUser(user);
        mv.addObject("msg", "You have registerd with us.");
		return mv;
	}


	@GetMapping(value = "/register")
	public String getRegisterPage(User user) {
	 
		return "register";
	}
	
	
}
